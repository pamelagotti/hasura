After the deploy of Hasura, give the permissions to the hasura user following

https://docs.hasura.io/1.0/graphql/manual/deployment/postgres-permissions.html#postgres-permissions

and set the permission on the schema for all the tables added after the initial deploy with

ALTER DEFAULT PRIVILEGES IN SCHEMA <schemaname>
GRANT SELECT ON TABLES TO hasurauser;
